import React, { useState } from 'react';
import axios from 'axios'

export default function LoginForm(props) {
    const [login, setLogin] = React.useState('');
    const [password, setPassword] = React.useState('');

    function handleSubmit (e) {
        e.preventDefault();

        const url = '/api/login';
        axios.post(url, {
            login: login, password: password
        })
            .then((resp) => {
                console.log(resp.data);
                alert('Token: ' + JSON.stringify(resp.data));
            })
            .catch(function (error) {
                console.error(error.response.data);
                alert('Error: ' + JSON.stringify(error.response.data));
            });
    };

    return (
         <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor='login'>Login</label>
                 <input type='login' id='login' value={props.login} onChange={(e) => setLogin(e.target.value)}  />
            </div>
            <div>
                <label htmlFor='password'>Password</label>
                 <input type='password' id='password' value={props.password} onChange={(e) => setPassword(e.target.value)}/>
            </div>
            <button>Login</button>
        </form>
    );
}
